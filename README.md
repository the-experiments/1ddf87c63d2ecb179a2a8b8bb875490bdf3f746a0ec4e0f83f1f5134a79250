
##	Observations, questions, etc.

Comments inline, but a few things I remember:

Should the Withdrawn value on an Account go negative when funds are transferred out? Have assumed yes.

More tests could be added.

There are a few text strings and numbers baked into the code that could/should be determined otherwise at runtime.

