using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moneybox.App;
using System;

namespace Moneybox.Tests
{

	//	for speed and because of the simplicity of the objects and operations involved,
	//	I've hardcoded bare minimum values into Account objects for these tests

	//	given more time there would be more test cases than this,
	//	for things like:
	//	the individual functions within the Account class - Withdraw, PayIn, etc.
	//	accounts not being returned by the account repository,
	//	accounts having no user
	//	users having no Email value,
	//	no notification service being provided
	//	and the Execute methods themselves

	[TestClass]
	public class TransferTests
	{

		[TestMethod]
		//	the exception string shouldn't really be baked into the test here.
		[ExpectedException(typeof(InvalidOperationException), "No source or destination account provided")]
		public void TestTransferWithNoAccounts()
		{

			Account.TransferFunds(null, null, 1.0m, null);
		}

		[TestMethod]
		//	the exception string shouldn't really be baked into the test here.
		[ExpectedException(typeof(InvalidOperationException), "Insufficient funds to make transfer")]
		public void TestWithdrawalWithInsufficientFunds()
		{

			Account from = new Account()
			{
				Balance = 1.0m,
				PaidIn = 0,
				Withdrawn = 0
			};

			Account.TransferFunds(from, null, 2.0m, null);
		}

		[TestMethod]
		//	the exception string shouldn't really be baked into the test here.
		[ExpectedException(typeof(InvalidOperationException), "Account pay in limit reached")]
		public void TestPayInOverLimit()
		{

			Account from = new Account()
			{
				Balance = 2.0m,
				PaidIn = 0,
				Withdrawn = 0
			};

			Account to = new Account()
			{
				Balance = 0m,
				PaidIn = Account.PayInLimit - 1.0m,
				Withdrawn = 0
			};

			Account.TransferFunds(from, to, 2.0m, null);
		}

		[TestMethod]
		public void TestSuccessfulTransferOfFunds()
		{

			Account from = new Account()
			{
				Balance = 10.0m,
				PaidIn = 0,
				Withdrawn = 0
			};

			Account to = new Account()
			{
				Balance = 3.0m,
				PaidIn = 1.0m,
				Withdrawn = 0
			};

			Account.TransferFunds(from, to, 4.0m, null);

			Assert.AreEqual(6.0m, from.Balance);
			Assert.AreEqual(0m, from.PaidIn);
			Assert.AreEqual(-4.0m, from.Withdrawn);	//	still not sure whether this go negative. Need to ask, but will work with it assuming yes for now
			Assert.AreEqual(7.0m, to.Balance);
			Assert.AreEqual(5.0m, to.PaidIn);
			Assert.AreEqual(0m, to.Withdrawn);
		}

		[TestMethod]
		public void TestPayInWithNoSourceAccount()
		{

			Account to = new Account()
			{
				Balance = 13.0m,
				PaidIn = 11.0m,
				Withdrawn = 0
			};

			Account.TransferFunds(null, to, 4.0m, null);

			Assert.AreEqual(17.0m, to.Balance);
			Assert.AreEqual(15.0m, to.PaidIn);
			Assert.AreEqual(0m, to.Withdrawn);
		}
	}
}
