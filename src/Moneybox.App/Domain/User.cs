﻿using Moneybox.App.Domain.Services;
using System;

namespace Moneybox.App
{
	public class User
	{
		public Guid Id { get; set; }

		public string Name { get; set; }

		public string Email { get; set; }

		public INotificationService notificationService { get; set; }

		public void NotifyFundsLow()
		{

			//	probably not the end of the world if the user isn't sent an email for this. There's no guarantee it would arrive in their inbox anyway
			if ((this.Email != null) && (this.notificationService != null)) {
				this.notificationService.NotifyFundsLow(this.Email);
			}
		}

		public void NotifyApproachingPayInLimit()
		{

			//	probably not the end of the world if the user isn't sent an email for this. There's no guarantee it would arrive in their inbox anyway
			if ((this.Email != null) && (this.notificationService != null)) {
				this.notificationService.NotifyApproachingPayInLimit(this.Email);
			}
		}
	}
}
