﻿using Moneybox.App.Domain.Services;
using System;

namespace Moneybox.App
{
	public class Account
	{
		//	4000m shouldn't really be baked into the code here, in case it changes
		public const decimal PayInLimit = 4000m;

		public Guid Id { get; set; }

		public User User { get; set; }

		public decimal Balance { get; set; }

		public decimal Withdrawn { get; set; }

		public decimal PaidIn { get; set; }

		public static void ValidateAccount(Account account, Guid accountId)
		{

			if (account == null) {
				//	the exception string shouldn't really be baked into the code here.
				throw new ArgumentException(string.Format("Account with ID {0} not found", accountId));
			}
		}

		private void SetNotificationService(INotificationService notificationService)
		{

			if (this.User != null) {
				this.User.notificationService = notificationService;
			}
		}

		private void CheckFundsAvailable(decimal amount)
		{

			var fromBalance = this.Balance - amount;
			//	0m potentially shouldn't be baked into the code here, in case there's a concept of per-account overdraft facilities later
			if (fromBalance < 0m) {
				//	the exception string shouldn't really be baked into the code here.
				throw new InvalidOperationException("Insufficient funds to make transfer");
			}
		}

		private void CheckFundsLow(decimal amount)
		{

			var fromBalance = this.Balance - amount;
			//	500m shouldn't really be baked into the code here
			if (fromBalance < 500m) {
				if (this.User != null) {
					this.User.NotifyFundsLow();
				}
			}
		}

		private void CheckPayInLimitReached(decimal amount)
		{

			var paidIn = this.PaidIn + amount;
			if (paidIn > Account.PayInLimit) {
				//	the exception string shouldn't really be baked into the code here.
				throw new InvalidOperationException("Account pay in limit reached");
			}
		}

		private void CheckPayInLimitApproaching(decimal amount)
		{

			var paidIn = this.PaidIn + amount;
			//	500m shouldn't really be baked into the code here
			if (Account.PayInLimit - paidIn < 500m) {
				if (this.User != null) {
					this.User.NotifyApproachingPayInLimit();
				}
			}
		}

		private void Withdraw(decimal amount)
		{

			this.Balance -= amount;
			this.Withdrawn -= amount;    //	would ask the question whether this value should go negative, but working on the assumption that it should for now
		}

		private void PayIn(decimal amount)
		{

			this.Balance += amount;
			this.PaidIn += amount;
		}

		public static void TransferFunds(Account from, Account to, decimal amount, INotificationService notificationService)
		{

			//	it's unknown whether it is acceptable to pay into an account without withdrawing from another, by any means within the system

			//	check that there is at least one account provided in case we've been called from somewhere we're not expecting without one
			if ((from == null) && (to == null)) {
				//	the exception string shouldn't really be baked into the code here.
				throw new InvalidOperationException("No source or destination account provided");
			}

			//	there is no check for both accounts being the same. it's entirely possible that is an acceptable scenario though
			if (from != null) {
				from.SetNotificationService(notificationService);
			}
			if (to != null) {
				to.SetNotificationService(notificationService);
			}

			if (from != null) {
				from.CheckFundsAvailable(amount);
			}
			if (to != null) {
				to.CheckPayInLimitReached(amount);
			}

			if (from != null) {
				from.CheckFundsLow(amount);
			}
			if (to != null) {
				to.CheckPayInLimitApproaching(amount);
			}

			if (from != null) {
				from.Withdraw(amount);
			}
			if (to != null) {
				to.PayIn(amount);
			}
		}

	}
}
